const MainScreen = "Main"
const EditScreen = "Edit"
const AddScreen = "Add"

export{
    MainScreen,
    EditScreen,
    AddScreen
};