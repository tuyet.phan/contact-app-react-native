
import React from 'react';
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import ContactApp from './components/Main'
import Edit from './components/Edit'
import Add from './components/Add'

const MainStack = createStackNavigator({
    MainScreen: {
        screen: ContactApp,
        navigationOptions: {
            header: null,
        }
    },
    EditScreen:
    {
        screen: Edit,        
    },
    AddScreen: {
        screen: Add,    
    },
},
)
const Container = createAppContainer(MainStack)
export default Container;
