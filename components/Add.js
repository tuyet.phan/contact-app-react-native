
/*Screen to add the contact*/
import React, { Component } from 'react';
import { TextInput, View, StyleSheet, Text, Image,Alert, TouchableOpacity } from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'ContactDatabase.db' });

export default class Add extends Component {
  constructor(props) {
    super(props);
    this.state = {      
      name: '',
      mobile: '',
      avatar: 'http://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png'
    };
    this.navigation = this.props.navigation;
  }
  static  navigationOptions = (
    {navigation}) => ( {
            headerTitleStyle: { textAlign: 'center', width: '100%', margin: 0 },
            headerTitle: 'Add Contact',
            headerLeft: (
                <TouchableOpacity
                    onPress={() => navigation.navigate('MainScreen')}
                >
                    <Image
                        style={styles.icon}
                        source={{uri: 'http://icons.iconarchive.com/icons/aha-soft/software/256/cancel-icon.png'}}>
                    </Image>
                </TouchableOpacity>

            ),
            headerRight: (
                <TouchableOpacity
                    onPress={ () => navigation.state.params.addContact()}
                >
                    <Image
                        style={styles.icon}
                        source={{uri: 'https://cdn2.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2/128/check-circle-green-512.png'}}>
                    </Image>
                </TouchableOpacity>

            ),
        })
  
  componentDidMount() {
    this.props.navigation.setParams({
      addContact: this.addContact.bind(this),
    });
  }

  addContact = () => {
    const {name, mobile, avatar} = this.state;

    if (!name){
      alert('Please fill  name');}

      else if(!mobile){
      alert('Please fill Contact number');
    }
    else {
      const navigation = this.props.navigation;
      db.transaction(function(tx) {
        tx.executeSql(
          'INSERT INTO table_contact (name,mobile, avata) VALUES (?,?,?)',
          [name, mobile, avatar],
          (tx, results) => {
            if (results.rowsAffected > 0) {
              Alert.alert(
                'Success',
                'Add Contact Successfully',
                [
                  {
                    text: 'Ok',
                    onPress: () => 
                     navigation.navigate("MainScreen")
                  },
                ],
                { cancelable: false }
              );
            }
             else {
              alert('Add contact Failed');
            }
          }
        );
      });
      
      // params.addContact(name, mobile, avatar);
  } }
 
  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        <View style={{
          height: "40%",
          width: "100%",
          backgroundColor: 'cyan',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <Image
            style={{
              height: "100%",
              width: "100%",
              resizeMode: 'contain'
            }}
            source={{
              uri: this.state.avatar
            }}
          >
          </Image>
        </View>
        <View style={{
          height: "60%",
        }}>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: 'pink',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontWeight: 'bold'
              }}
            > Name</Text>
            <TextInput
              style={this.searchbox}
              // keyboardType='default'
              placeholder='Enter name'
              autoFocus={true}
              onChangeText={name => this.setState({ name })}/>
          </View>
          <View style={{
            flexDirection: 'row',
            backgroundColor: 'yellow',
            alignItems: 'center',
          }}>
            <Text
              style={{
                fontWeight: 'bold'
              }}
            > Mobile</Text>

            <TextInput
              style={this.searchbox}
              // keyboardType='default'
              placeholder='Enter number'
              onChangeText={mobile => this.setState({ mobile })}/>
          </View>
        </View>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  searchbox:
  {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: '100%',
  },
  buton:
  {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    backgroundColor: 'green'
  },
  ButtonView: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
  },
  icon: {
    height: 30,
    width: 30,
    borderRadius: 15,
    paddingLeft: '3%',
    margin: 5,
},
})
