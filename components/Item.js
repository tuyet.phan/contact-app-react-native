import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image , Linking} from 'react-native';


import { withNavigation } from 'react-navigation';
const styles = StyleSheet.create({
    FloatlistView: {

        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'white',
        borderWidth: 2,
        borderColor: 'black',
        margin: 5
    },
    avatar: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
        overflow: 'visible',
        borderRadius: 1000,
    },
    FloatingButtonStyle: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
        overflow: 'visible',
        borderRadius: 1000,
    },

    FlatListItem: {
        padding: 10,
        fontSize: 16,
        color: 'black',
    }

})


class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
callContact = (mobile) =>{
    Linking.openURL(`tel:${mobile}`)
}
    render() {
        const { item } = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate("EditScreen", {
                    key: item.key,
                })}
                style={styles.FloatlistView}
            >
                <View
                    style={styles.FloatlistView}
                >
                    <View>
                        <Image
                            style={styles.avatar}
                            source={{ uri: item.avata }}>
                        </Image>
                    </View>
                    <View>
                        <Text style={styles.FlatListItem}>{item.name}</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        alignItems: "flex-end",
                    }}>
                        <TouchableOpacity
                        key = {item.key}
                        onPress={() => this.callContact(item.mobile)}>
                        <Image
                            style={styles.FloatingButtonStyle}
                            source={{ uri: 'https://icon-library.net/images/phone-call-icon-png/phone-call-icon-png-25.jpg' }}>
                        </Image>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
export default withNavigation(Item);
