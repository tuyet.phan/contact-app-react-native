/*Screen to update the contact*/
import React, { Component } from 'react';
import { TextInput, View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'ContactDatabase.db' });

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: '',
      name: '',
      avata: ''
    };
    this.navigation = this.props.navigation;
  }
  static navigationOptions = (
    { navigation }) => ({
      headerTitleStyle: { textAlign: 'center', width: '100%', margin: 0 },
      headerTitle: 'Edit Contact',
      headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.navigate('MainScreen')}
        >
          <Image
            style={styles.icon}
            source={{uri: 'http://icons.iconarchive.com/icons/aha-soft/software/256/cancel-icon.png'}}>
          </Image>
        </TouchableOpacity>

      ),
      headerRight: (
        <TouchableOpacity
          onPress={() => navigation.state.params.updateContact()}
        >
          <Image
            style={styles.icon}
            source={{uri: 'https://cdn2.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2/128/check-circle-green-512.png'}}>
          </Image>
        </TouchableOpacity>
      ),
    })
  componentDidMount() {
    this.props.navigation.setParams({
      updateContact: this.updateContact.bind(this),
    });
    this.getContact();}

getContact = () => {
  const { key } = this.navigation.state.params;
  db.transaction(tx => {
    tx.executeSql('SELECT * FROM table_contact where key = ? ', [key], (tx, results) => {
      this.setState({
        ...results.rows.item(0)
      })
    
    });
  });
}

  updateContact() {
    // var that = this;
    const { name, mobile, avata } = this.state;
    const { key } = this.navigation.state.params;
    const navigation = this.props.navigation;
    if (!name) {
      alert('Please fill  name');
    }
    else if (!mobile) {
      alert('Please fill Contact number');
    }
    else {
    db.transaction(function (tx) {
      tx.executeSql(
        'UPDATE table_contact set name=?, mobile=? where key=?',
        [name, mobile, key],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            Alert.alert('Success', 'Contact updated successfully',
              [
                {
                  text: 'Ok',
                  onPress: () => navigation.navigate('MainScreen')
                },
              ],
              { cancelable: false }
            );
          } else {
            alert('Updation Failed');
          }
        }
      );
    });
  }}

  DeleteContact = () => {
    const { key } = this.navigation.state.params;
    const navigation = this.props.navigation;

    db.transaction(tx => {
      tx.executeSql('DELETE FROM table_contact where key=?', [key], (tx, results) => {
      
        if (results.rowsAffected > 0) {
          Alert.alert(
            'Success',
            'Contact deleted successfully!',
            [
              {
                text: 'Ok',
                onPress: () => navigation.navigate('MainScreen'),
              },
            ],
            { cancelable: false },
          );
        } else {
          alert('Failed to delete contact!');
        }
      });
    });
  }

  render() {
    // this.navigation.state.params
    const { name, mobile, avata } = this.state;
    return (
      <View style={{
        flex: 1,

      }}>
        <View style={{
          height: "40%",
          width: "100%",
          justifyContent: 'center',
          alignItems: 'center',
          // backgroundColor: 'cyan'
        }}>
          <Image
            style={{
              height: "100%",
              width: "100%",
              resizeMode: 'contain'
            }}
            source={{ uri: this.state.avata }}
          >
          </Image>
        </View>
        <View style={{
          height: "60%",
        }}>
          <View style={{
            flexDirection: 'row',
            backgroundColor: 'pink',
            alignItems: 'center',
          }}>
            <Text
              style={{
                fontWeight: 'bold'
              }}> Name     </Text>

            <TextInput
              style={this.searchbox}
              keyboardType='default'
              placeholderTextColor='yellow'
              autoFocus={true}
              value={name}
              onChangeText={name => this.setState({ name })} />
          </View>
          <View style={{
            flexDirection: 'row',
            backgroundColor: 'yellow',
            alignItems: 'center',
          }}>
            <Text
              style={{
                fontWeight: 'bold'
              }}
            > Mobile   </Text>

            <TextInput
              style={this.searchbox}
              keyboardType='default'
              autoFocus={true}
              value={mobile}
              onChangeText={mobile => this.setState({ mobile })} />
          </View>
          <View style={[styles.flexCenter, styles.w100]}>
            <TouchableOpacity onPress={this.DeleteContact}>
              <Text style={[styles.deleteBtn, styles.textCenter]}>
                Delete
              </Text>
            </TouchableOpacity>
          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchbox:
  {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: '100%',
  },
  FloatingButtonStyle: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    overflow: 'visible',
    borderRadius: 1000,
  },
  icon: {
    height: 30,
    width: 30,
    borderRadius: 15,
    paddingLeft: '3%',
    margin: 5,
  },
  deleteBtn: {
    width: 150,
    padding: 10,
    backgroundColor: 'red',
    color: '#fff',
  }, textCenter: {
    textAlign: 'center',
  },
  flexCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w100: {
    width: '100%',
  },
  textCenter: {
    textAlign: 'center',
  },

})
