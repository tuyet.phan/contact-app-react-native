/**
 * Tuyet M. Phan
 * 2019.09.19
 */
import React, { Component } from 'react';
import {
  TextInput, StyleSheet, TouchableOpacity, View, Image, Text, FlatList,
} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import Item from './Item';
var db = openDatabase({ name: 'ContactDatabase.db' });

export default class ContactApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FlatListItems: [],
    };
    this.navigation = this.props.navigation;
  }

  componentDidMount() {
    this.getDatafromDB();
    this.props.navigation.setParams({
      searchContact: this.searchContact.bind(this),
    });
  }

  addContact = (name, mobile, avatar) => {
    db.transaction(function(tx) {
      tx.executeSql(
        'INSERT INTO table_contact (name,mobile, avata) VALUES (?,?,?)',
        [name, mobile, avatar],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            Alert.alert(
              'Success',
              'Add Contact Successfully',
              [
                {
                  text: 'Ok',
                  onPress: () => 
                   navigation.navigate("MainScreen")
                },
              ],
              { cancelable: false }
            );
          }
           else {
            alert('Add contact Failed');
          }
        }
      );
    });
  }

  getDatafromDB = () => {
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='table_contact'",
        [],
        function (tx, res) {
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS table_contact', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_contact(key INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(35), mobile VARCHAR(10), avatar VARCHAR(80))',
              []
            );
          }
        }
      );
    });

    db.transaction(tx => {
      tx.executeSql('SELECT * FROM table_contact', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        console.log("@@@@@", temp);
        
        this.setState({
          FlatListItems: temp,
        });
      });
    });
  }
  _onPressAddButton = () => {
    this.props.navigation.navigate('AddScreen',{
      addContact: this.addContact,
    })
  }

  _onPressEditButton = ({ name, mobile, avatar }) => {
    this.props.navigation.navigate('EditScreen', {
      name,
      mobile,
      avatar
    });
  }

  ListContact = (data) => (
    <FlatList
    data={data}
    renderItem={({ item }) => <Item item={item} />}
    keyExtractor={item => item.key}
    >
    </FlatList>

  );
searchContact(contact){
  if (contact.length > 0)
  {
    db.transaction(tx => {
      tx.executeSql(
        // "SELECT * FROM table_contact WHERE name LIKE '%' || ? || '%'",
         "SELECT * FROM table_contact WHERE name LIKE ?",
        [contact],
        (tx, results) => {
          var temp = [];
            //  console.log('#@:gdsgdshg ', results.rows.item(0));
          for (let i = 0; i < results.rows.length; ++i) {
            temp.push(results.rows.item(i));
          }
          // console.log('#@ahiahiaa: ', temp);
          this.setState({
            FlatListItems: temp,
          });
          // console.log('#@: ', this.state.records);
        },
      );
    });
  }
  else
  {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM table_contact', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        console.log("@@@@@", temp);        
        this.setState({
          FlatListItems: temp,
        });
      });
    });
  }

}
  render() {
    const preListContact = (
      <View style={[styles.w100, styles.h100, styles.flexCenter]}>
        <Text style={[styles.titleHeader, styles.w100, styles.textCenter]}>
          No contact!
          You can add contact now!
        </Text>
      </View>
    );

    return (
      <View style={{ flex: 1 }}>
        <View style={{
          height: 50,
          backgroundColor: 'cyan'
        }}>
          <TextInput style={this.searchbox}
            // keyboardType='default'
            placeholder='Search...'
            // autoFocus={true} 
            onChangeText={contact => this.searchContact(contact)}
            />
        </View>
        <View style={{
          flex: 100,
          backgroundColor: "pink"
        }}>
          <View
            style={{
              flex: 1,
              flexDirection: "column"
            }}
            // item={item}
            onClick={this._onPressEditButton}
            // index={index}
          >
            {this.state.FlatListItems.length > 0 ? this.ListContact(this.state.FlatListItems) : preListContact}
          </View>

        </View >
        <TouchableOpacity onPress={this._onPressAddButton}
          style={styles.buton}
        >
          <View
            style={styles.ButtonView}
          >
            <Image
              style={styles.FloatingButtonStyle}
              source={{uri: 'https://icon-library.net/images/white-plus-icon/white-plus-icon-21.jpg'}}
            />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  textCenter: {
    textAlign: 'center',
  },
  titleHeader: {
    fontSize: 20,
  },
  flexCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w100: {
    width: '100%',
  },
  h100: {
    height: '100%',
  },
  container: {
    flex: 1,
    paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 10,
    height: 44,
  },
  TouchableOpacityStyle: {
    backgroundColor: 'red',
    position: 'absolute',
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    flex: 1
  },

  FloatingButtonStyle: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    overflow: 'visible',
    borderRadius: 1000,
  },
  FlatListItem: {
    padding: 10,
    fontSize: 16,
    color: 'black',
  }
  ,
  avatar: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    overflow: 'visible',
    borderRadius: 1000,
  },
  FloatlistView: {

    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: 'black',
    margin: 5
  },
  buton:
  {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    // backgroundColor: 'green'
  },
  ButtonView: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
  },
  searchbox:
  {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: '100%',
  }

});
