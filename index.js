import { AppRegistry } from 'react-native';
import Container from './router'
import { name as appName } from './app.json';


AppRegistry.registerComponent(appName, () => Container);
